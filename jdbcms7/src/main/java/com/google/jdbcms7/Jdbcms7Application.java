package com.google.jdbcms7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jdbcms7Application {

	public static void main(String[] args) {
		SpringApplication.run(Jdbcms7Application.class, args);
	}

}
